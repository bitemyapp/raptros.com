+++
title = "about"

[extra]
notitle = true
+++

i'm aidan l. coyne.

* software engineer, mostly work in java.
* hobby projects are in Rust these days.
* i listen to a lot of metal.
* my blog will touch on such subjects as
    * programming
    * metal
    * random stuff
