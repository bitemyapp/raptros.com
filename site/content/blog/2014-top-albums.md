+++
title = "favorite albums of the year 2014"
date = 2015-01-01
+++

the list i've assembled this year is a list of my 15 favorite albums (or other
releases) of the year. 
i think it's important to make it clear that this is not the same as
"what do i think are the best albums of the year". i would not be comfortable
attempting to assemble and defend a list of the "best of the year", for three
key reasons - 

* *exposure*. i have not heard every metal album released this year.  i probably
  haven't heard even a tenth of the albums released this year. the albums i have
  added to my collection this year have gone through a hell of a journey to get
  there - they made it onto the various and dubious blogs i read (or even worse,
  facebook pages i happen to glance at occasionally), received descriptions that
  interested me enough to hit play on whatever streams were linked, and reached
  me in the right mood for me to actually commit to obtaining the album (or
  EP or whatever). it's a long and stupid process, and it means i miss out on a
  lot potentially great releases.
* *current understanding* - i.e. what i currently know about the objective
  evaluation of music in general, and metal in particular, and how this informs
  what i listen to and how this informs my ...
* *personal taste*, which is not necessarily related to quality or timelessness
  or anything beyond sheer personal bias. personal taste is what makes
  me listen to certain albums over and over, what informs my immediate reaction
  to anything i hear, and what lets me enjoy all kinds of dumb albums.

on the other hand, the task of listing the albums i enjoyed the most in 2014 is
only as hard as it is for me to remember what albums (or EPs, or whatever) I
enjoyed the most this year.

anyway, now that i've thouroughly confused the subject, i just have a few notes
on the organization of this list before i get to the meat.

<!--more-->

## organization

in the interest of making this page load reasonably well, i am not directly
embedding streams this time around. i'm putting in album art and links out to
streams, videos, and label sites as necessary. along with that, i'm also
including what i think the genre and style are for each album, along with some
words about each.

i've divided up my list into 4 loose categories that don't necessarily relate to
anything about the releases listed in each section, but will hopefully make this
more navigable. neither the ordering of the sections or the albums within the
sections reflect any meaningful preference on my part, though maybe the last
section has my favorites.

* [blackened](#blackened)
* [have some fun](#have-some-fun)
* [slower or moodier](#slower-or-moodier)
* [did i call it or what](#did-i-call-it-or-what)


## blackened

### Behemoth - *The Satanist*

{{ statimg(class="cover", alt="Behemoth - The Satanist", post="2014-top-albums", img="behemoth-the-satanist.jpg") }}

* genre: blackened death metal
* style: polish
* stream: [youtube video](http://youtu.be/Cfa9z8qXkgQ)
* order: [amazon](http://www.amazon.com/Satanist-Behemoth/dp/B00HAH7H5W/)

i saw these guys live back in May. i'm not sure that i would have gone if i
hadn't listened to this album, which rekindled my interest both in Behemoth as a
band and black metal as a genre.

### Noneuclid - *Metatheosis*

{{ statimg(class="cover", alt="Noneuclid - Metatheosis", post="2014-top-albums", img="noneuclid-metatheosis.jpg") }}

* genre: death/thrash
* style: blackened, occasionally doomy
* stream/order: [bandcamp](https://blood-music.bandcamp.com/album/metatheosis)

this came out of nowhere and blew me away. it's a weird album.

### Separatist - *Closure*

{{ statimg(class="cover", alt="Separatist - Closure", post="2014-top-albums", img="separatist-closure.jpg") }}

* genre: ... blackened deathcore  (wtf)
* style: ambient?
* stream/order: [bandcamp](https://separatistdm.bandcamp.com/album/closure)

it's not symphonic, at all, which puts it way ahead of other attempts to blend
a deathcore sound and a blackened sound. it also isn't an embarrasment of
breakdowns.

### Warforged - *Essence of the Land*

{{ statimg(class="cover", alt="Warforged - Essence of the Land", post="2014-top-albums", img="warforged-essence-of-the-land.jpg") }}
<img class="cover" alt="" src=""/>

* genre: blackened deathcore (also wtf)
* style: ambient.
* stream/order: [bandcamp](https://totaldeathcore.bandcamp.com/album/essence-of-the-land)

it's only an EP, but it's a great one. actually, without this EP i would not
believe in the genre's possibility.

## have some fun

### Xerath - *III*

<img class="cover" alt="Xerath - III" src="/images/xerath-iii.png"/>

* genre: djent
* style: symphonic djent
* stream/order: [bandcamp](https://candlelightrecordsusa.bandcamp.com/album/iii)

these guys continue to work hard on making symphonic djent a thing, and this
time i actually enjoyed the whole album.

### A Sense of Gravity - *Travail*

{{ statimg(class="cover", alt="A Sense of Gravity - Travail", post="2014-top-albums", img="a-sense-of-gravity-travail.jpg") }}

* genre: prog metal
* style: grooves (such riffs, wow)
* stream/order: [bandcamp](https://asenseofgravity.bandcamp.com/album/travail)

it's fundamentally a prog metal album, but instead of being power metal styled,
it's got more of a death metal or groove metal sound.

### Allegaeon - *Elements of the Infinite*

{{ statimg(class="cover", alt="Allegaeon - Elements of the Infinite", post="2014-top-albums", img="allegaeon-elements-of-the-infinite.jpg") }}

* genre: melodic/technical death metal
* style: scifi
* videos:
    * [1.618](https://www.youtube.com/watch?v=ErhgZhhXPvA) 
    * [Thresholds of Perception](https://www.youtube.com/watch?v=Sx9NLiA9Kk8) 
    * [Our Cosmic Casket](https://www.youtube.com/watch?v=SIqt_BT-ahM)
* order: 
    * [through label](http://www.metalblade.com/allegaeon/)
    * [amazon](http://www.amazon.com/Elements-Infinite-Allegaeon/dp/B00K5P3BM4/)

hell yeah! these guys are awesome!

### Bloodshot Dawn - *Demons*

{{ statimg(class="cover", alt="Bloodshot Dawn - Demons", post="2014-top-albums", img="bloodshot-dawn-demons.jpg") }}

* genre: melodic/technical death metal
* style: awesome solos
* stream/order: [bandcamp](https://bloodshotdawn.bandcamp.com/album/demons)

they've got more solos than the Allegaeon album, but it's less groove oriented.
still awesome.

### Exmortus - *Slave to the Sword*

{{ statimg(class="cover", alt="Exmortus - Slave to the Sword", post="2014-top-albums", img="exmortus-slave-to-the-sword.jpg") }}

* genre: thrash metal
* style: so much shredding
* stream/order: [bandcamp](https://exmortus-official.bandcamp.com/album/slave-to-the-sword)

i'd never heard of Exmortus until i saw them in SF, opening for Omnium Gatherum
and Dark Tranquillity. this isn't rethrash, this is straight up thrash with
aggression and wild guitars.

### Arsafes - *Ratocracy*

{{ statimg(class="cover", alt="Arsafes - Ratocracy", post="2014-top-albums", img="arsafes-ratocracy.jpg") }}

* genre: industrial/groove metal
* style: think strapping young lad, except russian and groove
* stream/order: [bandcamp](https://arsafes.bandcamp.com/album/ratocracy)

i think i've said everything that needs to be said about them (except that the
lyrics are unintentionally hilarious.)

## slower or moodier

### Stealing Axion - *Aeons*

{{ statimg(class="cover", alt="Stealing Axion - Aeons", post="2014-top-albums", img="stealing-axion-aeons.jpg") }}

* genre: djent
* style: death/doom
* stream/order: [bandcamp](https://stealingaxion.bandcamp.com/album/aeons)

these guys got way heavier, darker, and moodier than on their first album and it
worked out amazingly well.

### Woccon - *Solace In Decay*

{{ statimg(class="cover", alt="Woccon - Solace In Decay", post="2014-top-albums", img="woccon-solace-in-decay.jpg") }}

* genre: death/doom
* style: not finnish
* stream/order: [bandcamp](https://deathboundrecords.bandcamp.com/album/solace-in-decay)

they sound like bands like Swallow the Sun and Insomnium and others with that
Finnish melodic death/doom sound ... except not. apparently they used to do more
of a black metal thing.

### Insomnium - *Shadows of the Dying Sun*

{{ statimg(class="cover", alt="Insomnium - Shadows of the Dying Sun", post="2014-top-albums", img="insomnium-shadows-of-the-dying-sun.jpg") }}

* genre: melodic death metal
* style: finnish
* videos:
    * [While We Sleep](https://www.youtube.com/watch?v=vBZ5SLJmfdw)
    * [Revelation](https://www.youtube.com/watch?v=bdB-8JlMgN8)
* stream: [youtube](https://www.youtube.com/watch?v=icqddnGuijs)
* order: 
    * [webstore](http://www.madsupply.com/en/shop/band+merchandise/insomnium)
    * [amazon](http://www.amazon.com/Shadows-Dying-Sun-Insomnium/dp/B00IQFBZU0/)

it's an Insomnium album. oh, it continues the trend of Insomnium albums getting
less despairing.

## did i call it or what

### Black Crown Initiate - *The Wreckage of Stars*

{{ statimg(class="cover", alt="Black Crown Initiate - The Wreckage of Stars", post="2014-top-albums", img="black-crown-initiate-the-wreckage-of-stars.jpg") }}

* genre: progressive death metal
* style: yes
* stream: [youtube](https://www.youtube.com/watch?v=3pqXRRWUGHA)
* order: [amazon](http://www.amazon.com/Wreckage-Stars-Black-Crown-Initiate/dp/B00MPJQ5J4/)

i said these guys would have an awesome debut album. oh, they're great live,
too.

### Fallujah - *The Flesh Prevails*

{{ statimg(class="cover", alt="Fallujah - The Flesh Prevails", post="2014-top-albums", img="fallujah-the-flesh-prevails.jpg") }}

* genre: progressive death metal
* style: ambient
* stream: [youtube](https://www.youtube.com/watch?v=UyRW8FTlp7o)
* order: [amazon](http://www.amazon.com/Flesh-Prevails-Fallujah/dp/B00KJFZW0O)

i think i've listened to this album more than any other this year. it's awesome
and entrancing.
