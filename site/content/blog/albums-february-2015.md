+++
title = "albums for february 2015"
date = 2015-03-03
+++

<!--more-->

## Inquisition - Obscure Verses for the Multiverse
* released: october 2013
* genre: black metal
* get: [bandcamp](https://inquisitionbm.bandcamp.com/album/obscure-verses-for-the-multiverse)
* comment: i've heard Inquisition before. I've even seen them live. I just never got around to adding this album to my collection.

## Sarpanitum - Blessed Be My Brothers
* released: february 2015
* genre: death metal
* style: melodic and technical
* get: [bandcamp](https://willowtip.bandcamp.com/album/blessed-be-my-brothers)

## Psygnosis - Human Be[ing]
* released: march 2014
* genre: death metal
* style: electronic, wild
* get: [bandcamp](https://psygnosis.bandcamp.com/album/human-be-ing)

## Armaggedon - Captivity and Devourment
* released: january 2015
* genre: melodic death metal
* get [bandcamp](https://listenable-records.bandcamp.com/album/captivity-and-devourment-2)

## Keep of Kalessin - Epistemology
* released: february 2015
* genre: melodic death metal
* style: blackened
* fullstream: [metalhammer, for now](http://metalhammer.teamrock.com/news/2015-02-12/norway-s-epic-metallers-keep-of-kalessin-stream-new-album-in-full)
* order: [indie recordings?](http://www.omerch.eu/shop/indierecordings/proddetail.php?prod=Omerch_INDIE119CD)

## Svart Crown - Profane
* released: april 2013
* genre: blackened death metal
* style: somewhat sludgy
* get: [bandcamp](https://svartcrown.bandcamp.com/album/profane)

## Prey for Nothing - The Reasoning
* released: november 2014
* genre: melodic death metal
* get: [band's giving out free download on their site](http://www.preyfornothing.com/)

## Ensiferum - One Man Army
* released: february 2015
* genre: folk metal
* get: wherever


## Escher - The Ground is Missing
* released: january 2015
* length: ep
* genre djentcore (?)
* get: [bandcamp](https://escher.bandcamp.com/album/the-ground-is-missing)

## Distance - -I-
* released: november 2014
* genre: progressive death metal
* get: [bandcamp](https://dist4nce.bandcamp.com/album/i)

## Encircle - Lost Chronicles
* released: january 2015
* genre: tech death
* get: [bandcamp](https://encircle.bandcamp.com/album/lost-chronicles)


