+++
title = "albums for june 2015"
date = 2015-06-04
+++

there's a lot of releases coming up this month.

<!--more-->

## Leprous - The Congregation

{{ statimg(class="cover", post="albums-june-2015", img="leprous-the-congregation.jpg") }}

* released: june 2015
* genre: prog metal
* style: norwegian, moody
* get: [amazon](http://www.amazon.com/Congregation-Leprous/dp/B00JFRKPOK)

## Dorededuh - Dar De Duh

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/dordeduh-dar-de-duh.jpg"/>

* released: september 2012
* genre: black metal
* style: atmospheric
* get: [bandcamp](https://dordeduh.bandcamp.com/album/dar-de-duh-2)

## Gorgoroth - Instinctus Bestialis

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/gorgoroth-instinctus-bestialis.jpg"/>

* released: june 2015
* genre: black metal
* get: [google](https://play.google.com/store/music/album/Gorgoroth_Instinctus_Bestialis?id=Bttfs4dldnmzrwwxdxx5wvvd4ke)


## Earth - Hex; Or Printing In The Infernal Method

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/earth-hex.jpg"/>

* released: january 2005
* genre: drone metal
* style: country/western
* get: [bandcamp](https://earthsl.bandcamp.com/album/hex-or-printing-in-the-infernal-method)


## Wrvth - Wrvth

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/wrvth-wrvth.jpg"/>

* released: june 2015
* genre: tech death
* get: [google](https://play.google.com/store/music/artist/Wrvth?id=Alsq5ypakqoanmtfuyte6rjwhgq)

## Midnight Odyssey - Shards of Silver Fade

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/midnight-odyssey-shards-of-silver-fade.jpg"/>

* released: june 2015
* genre: ambient black metal
* get: [bandcamp](https://i-voidhangerrecords.bandcamp.com/album/shards-of-silver-fade)


## Abyssal - Antikatastaseis

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/abyssal-antikatastaseis.jpg" />

* released: june 2015
* genre: death metal
* style: blackened, doom
* get: [bandcamp](https://abyssal-home.bandcamp.com/album/antikatastaseis)

## Paradise Lost - The Plague Within

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/paradise-lost-the-plague-within.jpg" />

* released: june 2015
* genre: death/doom
* get: [google play](https://play.google.com/store/music/album/Paradise_Lost_The_Plague_Within?id=Bvdkh433laqsglzyd4eshjtn6xa)

## Dew-Scented - Intermination

<img class="cover" src="http://static.raptros.com/images/albums-june-2015/dew-scented-intermination.jpg" />

* released: june 2015
* genre: thrash metal
* get: [google play](https://play.google.com/store/music/album/Dew_Scented_Intermination?id=Bzxvyglaimnjiscbd74u6msljwu)


