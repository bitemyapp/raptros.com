+++
title = "albums for may 2015"
date = 2015-05-31
+++

added all but the first of these albums today.

<!-- more -->

## Earth - Earth 2: Special Low Frequency Version

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/earth-2-special-low-frequency-version.jpg"/>

* released: 1992
* genre: drone metal

## Feared - Synder

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/feared-synder.jpg"/>

* released: may 2015
* genre: groove/melodic death metal
* get: [band site](http://fearedband.com/store-feared-synder)

## Disarmonia Mundi - The Isolation Game

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/disarmonia-mundi-the-isolation-game.jpg"/>

* released: december 2009
* genre: melodic death metal

## Thrown to the Sun - Out of Themselves Things Will Come 

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/thrown-to-the-sun-out-of-themselves-things-will-come.jpg"/>

* released: december 2014
* genre: progressive death metal
* get: [bandcamp](https://throwntothesun.bandcamp.com/)

## Distant Sun - Dark Matter

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/distant-sun-dark-matter.jpg"/>

* released: march 2015
* genre: thrash metal
* get: [bandcamp](https://metalism.bandcamp.com/album/dark-matter)

## Wardaemonic - Obsequium

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/wardaemonic-obsequium.jpg"/>

* released: march 2015
* genre: blackened death metal
* get: [bandcamp](https://wardaemonic.bandcamp.com/album/obsequium-lp)

## Maladie - Still

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/maladie-still.jpg"/>

* released: march 2015
* genre: progressive black metal
* get: [google](https://play.google.com/store/music/album/Maladie_Still?id=Bbejnf2acztksgce3ouiagjupgy)
* stream: [soundcloud has the track Inexistentia](https://soundcloud.com/apostasy-records/maladie-inexistentia)

## Deathblow - The Other Side of Darkness

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/deathblow-the-other-side-of-darkness.jpg"/>

* released: april 2015
* genre: thrash metal
* get: [bandcamp](https://deathblow1.bandcamp.com/album/the-other-side-of-darkness)

## Obsequiae - Aria of Vernal Tombs

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/obsequiae-aria-of-vernal-tombs.jpg"/>

* released: may 2015
* genre: black metal
* style: atmospheric, medieval (?)
* get: [20buckspin bandcamp](http://listen.20buckspin.com/album/aria-of-vernal-tombs-2)

## The Monolith Deathcult - Bloodcvlts

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/the-monolith-deathcult-bloodcults.jpg"/>

* released: march 2015
* genre: death metal
* get: [bandcamp](https://themonolithdeathcult.bandcamp.com/album/bloodcults)

## Vintage Warlords - The Invisible Foe

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/vintage-warlords-the-invisible-foe.jpg"/>

* released: may 2015
* genre: death metal
* style: slow, stompy
* get: [bandcamp](https://redefiningdarknessrecords.bandcamp.com/album/the-invisible-foe-ep)

## Zatokrev - Silk Spiders Underwater

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/zatokrev-silk-spiders-underwater.jpg"/>

* released: april 2015
* genre: doom metal
* style: blackened
* get: [bandcamp](https://candlelightrecordsusa.bandcamp.com/album/silk-spiders-underwater-2)

## Dawn of Azazel - The Tides of Damocles

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/dawn-of-azazel-the-tides-of-damocles.jpg"/>

* released: april 2015
* genre: blackened death metal
* get: [bandcamp](https://dawnofazazel.bandcamp.com/album/the-tides-of-damocles)

## Mare Infinitum - Alien Monolith God

<img class="cover" src="http://static.raptros.com/images/albums-may-2015/mare-infinitum-alien-monolith-god.jpg"/>

* released: april 2015
* genre: death/doom
* style: atmospheric
* theme: lovecraftian
* get: [bandcamp](https://mareinfinitum.bandcamp.com/album/alien-monolith-god)
