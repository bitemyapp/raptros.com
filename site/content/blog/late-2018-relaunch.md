+++
title = "raptros.com relaunched!"
date = 2018-12-19
+++

Announcing the overhaul of the infrastructure for raptros.com!

I've managed to replace a cumbersome and not-well-planned setup based on github
pages and hakyll with a new setup based on [Zola][], Gitlab, and S3.

This new setup has automatic deployment as well as a preview-site setup, which
means it should be much easier for me to add new content.

You can see the source code for the new site in my [raptros.com gitlab
repository](https://gitlab.com/raptros/raptros.com/).

<!-- more -->

goals
-----

My key goals in figuring out the new infrastructure were:
* Automatic deployment. The previous setup, which used hakyll and github pages,
  was cumbersome to update.
* hosting in S3 - this gives me better control and more flexibility.
* easier generation model. Hackyll is flexible and powerful, but it honestly
  gave me more than I needed.

I also had some additional goals:
* Get more hands-on experience with Gitlab's CI/CD tooling.
* Practice setting up docker images.
* try out [Zola][]

setup
-----

Here's what I've done:
* I'm using [Zola][] as the static site generator. I think it's flexible enough
  for my purposes, and I think it's both flexible enough and reasonably
  straightforward enough for future uses.
* I've set up some S3 buckets as static site hosts, and pointed my DNS at them.
* I wrote out a couple of Makefiles to store the build and deploy operations,
  i.e. `make` handles:
    * running Zola to build the site in both prod and staging configurations,
      as well as for local testing
    * performing the awscli commands that upload the generated site to the
      correct locations
    * constructing a docker image
* the Docker image I just mentioned is the one I published to my gitlab repo
    * it has Zola and the awscli installed
    * as well as other tools needed to be run by Gitlab
* Gitlab can therefore run the docker image, check out the repo onto it, and
  run the make commands needed to update the site appropriately.
    * not only does this include publishing raptros.com on updates to master,
    * but also it can deploy branches to a staging URL, allowing me to preview
      the generated site

caveats
-------

one of the downsides of this move is that Zola only supports a single rss feed
- at least for now. hopefully it'll have support for multiple, custom format
feeds, and i'll be able to bring back the atom feed.

[Zola]: http://www.getzola.org/
