+++
title = "contact"

[extra]
notitle = true
+++

get in touch!

email me
--------
i can be reached at a number of emails

* [coynea90@gmail.com](mailto:coynea90@gmail.com)
* [alc@raptros.com](mailto:alc@raptros.com)

telegram
--------

you can find me on telegram as @raptros

other sites
-----------
* [my github](https://github.com/raptros)
* [my gitlab](https://gitlab/com/raptros)
