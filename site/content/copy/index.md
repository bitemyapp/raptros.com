+++
title = "copyright/license info"

[extra]
notitle = true
+++

&copy; 2018-2019 aidan l. coyne.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
    <img alt="Creative Commons License" style="border-width:0" src="cc-by-nc-nd.png" />
</a>

The pages and original content of 
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">raptros.com</span>, published by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">aidan l coyne</span> are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.


the code and infrastructure, found in the [raptros.com gitlab
repo](http://gitlab.com/raptros/raptros.com/) is licensed according to the
details found in the LICENSE file.
